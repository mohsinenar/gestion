// Import Vue
import Vue from 'vue'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

import login from './components/login.vue'
import router from './router'


Vue.use(Vuetify)

Vue.config.productionTip = false

// Import App Custom Styles
// import AppStyles from './assets/sass/main.scss'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data:{
      drawer: null
  },
  components: { login }
})
