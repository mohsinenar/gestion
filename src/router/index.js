import Vue from 'vue'
import Router from 'vue-router'


import login from '@/components/login.vue'
import home from '@/components/home.vue'
import addActivity from '@/components/addActivity.vue'
import Listcandidats from '@/components/Listcandidats.vue'
import etapes from '@/components/etapes.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: login
    },
  	{ 
  		path: '/',
  		name:'home',
  		component: home
  	},
    { 
      path: '/List-candidats',
      name:'list candidats',
      component:Listcandidats
    },
    { 
      path: '/etapes',
      name:'list des etapes',
      component:etapes
    }
  ]
})
