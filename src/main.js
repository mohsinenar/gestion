import Vue from 'vue'
import 'vuetify/dist/vuetify.css'
import Vuetify from 'vuetify'
import VueCordova from 'vue-cordova'
import VueHead from 'vue-head'
import router from './router'
import store from './veux'

Vue.use(Vuetify, {
  rtl: true
})

Vue.config.productionTip = true
Vue.use(VueCordova)
Vue.use(VueHead)


// add cordova.js only if serving the app through file://
if (window.location.protocol === 'file:' || window.location.port === '3000') {
  var cordovaScript = document.createElement('script')
  cordovaScript.setAttribute('type', 'text/javascript')
  cordovaScript.setAttribute('src', 'cordova.js')
  document.body.appendChild(cordovaScript)
}



/* eslint-disable no-new */
new Vue({
    store,
    computed: {
    clients () {
      return store.state.clients
    }
  },
  data:{
    drawer:null,
    routes: [
    { 
      path: '/',
      name:'home',
      icon:'home'
    },
    {
      path: '/List-candidats',
      name: 'list candidats',
      icon:' add_box'
    },
    {
      path: '/login',
      name: 'login',
      icon:'supervised_user_circle'
    },
    {
      path: '/etapes',
      name: 'etapes',
      icon:'supervised_user_circle'
    }
  ]
  },
  el: '#app',
  router,
  head: {
    meta: [
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover'
      }
    ]
  }
})
