import Vue from 'vue'
import Vuex from 'vuex'
import 'es6-promise/auto'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    candidats:[
    	{
        name:"mouhcine narhmouche",
        image:"https://picsum.photos/200/200?image=23",
        id:1
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=24",
        id:2
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=32",
        id:3
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      }, {
        name:"المرحلة 1 ",
        image:"https://picsum.photos/200/200?image=31",
        id:4
      },
    ],
    etapes:[
      {description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, quis! ",name:"المرحلة 1 "},
      {description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, nobis. ",name:"المرحلة 2 "},
      {description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam, reiciendis. ",name:"المرحلة 3 "},
    ]
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})
